import 'package:flutter/material.dart';
import 'package:tempo/CarList.dart';
import 'package:tempo/LogoutDrawer.dart';
//import 'package:tempo/MenuHorizontalScroll.dart';
import 'package:tempo/NotificationDrawer.dart';
import 'package:tempo/PostNewService.dart';
import 'package:tempo/PostaddDrawer.dart';
import 'package:tempo/ProfileDrawer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tempo/ShowallPosts.dart';
import 'package:tempo/UserProfile.dart';
import 'package:tempo/YourOwnPostDrawer.dart';

Future<String>getNamePreferences() async{
  SharedPreferences prefs= await SharedPreferences.getInstance();
  String name=prefs.getString("name");
  return name;
}
Future<String>getNamePreferences2() async{
  SharedPreferences prefs= await SharedPreferences.getInstance();
  String name=prefs.getString("userprofilepath");
  return name;
}

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  String userprofilepath="https://i.ibb.co/SRypvcM/userpath.jpg";String name="";
  List<String> img=["https://i.ibb.co/xJhbKZt/car.png","https://i.ibb.co/Qd5bCgH/bikegrid.png",
    "https://i.ibb.co/rMLbcHf/smartphonegrid.png","https://i.ibb.co/ydR5sQW/homegrid.png",
    "https://i.ibb.co/qd7Sw3K/shirtgrid.png","https://i.ibb.co/9YqX78w/laptop.png","https://i.ibb.co/7CF3vTY/catering.png"
    ,"https://i.ibb.co/WfBLJFg/shop.png","https://i.ibb.co/GFtnGYm/nightstand.png","https://i.ibb.co/9pt17Ys/royal-palace.png"];
  List<String> cat=["Car","Bike","Mobile","House","Clothes","Laptops","Catering","Shops","Furniture","Palace"];
  int _cIndex = 0;

  void _incrementTab(index) {
    setState(() {
      _cIndex = index;
    });
  }

  @override
  void initState() {
     getNamePreferences2().then(updateuserprofilepath);
     getNamePreferences().then(updatename); 
     super.initState();
   }
   void updateuserprofilepath(String userprofilepath){
    setState(() {
      this.userprofilepath=userprofilepath;
    });
  }
  void updatename(String name){
    setState(() {
      this.name=name;
    });
  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text("Categories"),backgroundColor:Colors.deepOrange,
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Colors.deepOrange[800],
                    Colors.deepOrange,
                    Colors.deepOrange[400],
                  ]
                ),
              ),
              child: Container(
                child: GestureDetector(
                  onTap: (){
                    Route route=MaterialPageRoute(builder: (context)=>UserProfile());
                    Navigator.push(context, route);
                  },
                    child: Column(
                    children: <Widget>[
                      Container(
                        height: 100,width: 100,
                        child: ClipOval(
                          child: Image(
                            image: NetworkImage(userprofilepath),
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      Text("$name"),
                    ],
                  ),
                ),
              ),
            ),
            ProfileDrawer(),
            YourOwnPostDrawer(),
            NotificationDrawer(),
            PostaddDrawer(),
            LogoutDrawer(),
          ],
        ),
      ),
      body: GridView.count(
              crossAxisCount: 3,
              children:List.generate(10, (index)
              {
                return Card(
                  child: InkWell(
                    splashColor: Colors.black54,
                    onTap: (){
                      Navigator.push(context,MaterialPageRoute(builder: (context) => CarList(),),);
                    },
                      child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 25),
                            child: Image(
                            image: NetworkImage(img[index]),
                            height: 60,width: 60,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10),
                          child: Text(
                            cat[index],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }
            ),
          ),
      bottomNavigationBar:BottomNavigationBar(
        currentIndex: _cIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: new Text('Home')
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add),
            title: new Text('Add New Post')
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.menu),
            title: new Text('All Posts')
          )
        ],
        onTap: (index){
            _incrementTab(index);
            //print(_cIndex);
            if(_cIndex==1)
            {
              Route route=MaterialPageRoute(builder: (context)=>PostNewService());
              Navigator.push(context, route);
            }
            if(_cIndex==2)
            {
                Route route=MaterialPageRoute(builder: (context)=>ShowAllPosts());
                Navigator.push(context, route);
            }
        },
      ), 
    );
  }
}






