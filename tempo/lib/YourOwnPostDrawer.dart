import 'package:flutter/material.dart';
import 'package:tempo/YourOwnPost.dart';

class YourOwnPostDrawer extends StatefulWidget {
  @override
  _YourOwnPostDrawerState createState() => _YourOwnPostDrawerState();
}

class _YourOwnPostDrawerState extends State<YourOwnPostDrawer> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
      child:Container(
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color:Colors.grey ))
        ),
        child:  InkWell(
          splashColor:Colors.orangeAccent ,
          onTap: ()=>{
             Navigator.push(context,MaterialPageRoute(builder: (context) => YourOwnPost(),),),
          },
          child: Container(
            height: 55,
            child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(Icons.shopping_cart),
                    Text("    Your Own Post",
                    style:TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
              Icon(Icons.arrow_right),
            ],
          ),
        ),
      ),
      ),
    );
  }
}