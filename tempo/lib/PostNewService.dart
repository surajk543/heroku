import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<String>getNamePreferences() async{
  SharedPreferences prefs= await SharedPreferences.getInstance();
  String name=prefs.getString("userid");
  return name;
}

Future<String>getNamePreferences2() async{
  SharedPreferences prefs= await SharedPreferences.getInstance();
  String name=prefs.getString("name");
  return name;
}

class PostNewService extends StatefulWidget {
  @override
  _PostNewServiceState createState() => _PostNewServiceState();
}

class _PostNewServiceState extends State<PostNewService> {
  List<String>naming=["Heading Name*","Price*","City*","Country*","Pincode*","About*"];
  var currentstate='Andhra Pradesh';
  var currentcategoryname="Car";
  String username="";
  var category=["Car","Bike","Clothes","House","Mobile","Laptops","Catering","Shops","Furniture","Palace"];
  var currentday="1";
  var rentdays=["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20",
    "21","22","23","24","25","26","27","28","29","30"];
  var currenthours="1";
  var renthours=["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20",
    "21","22","23","24"];
  var currentminutes="1";
  var rentminutes=["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20",
    "21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41"
     ,"42","43","44","45","46","47","48","49","40","50","51","52","53","54","55","56","57","58","59"];
  var state=[
	 'Andhra Pradesh',
	 'Arunachal Pradesh',
	 'Assam',
	 'Bihar',
	 'Chhattisgarh',
	 'Goa',
	 'Gujarat',
	 'Haryana',
	 'Himachal Pradesh',
	 'Jammu & Kashmir',
	 'Jharkhand',
	 'Karnataka',
	 'Kerala',
	 'Madhya Pradesh',
	 'Maharashtra',
	 'Manipur',
	 'Meghalaya',
	 'Mizoram',
	 'Nagaland',
	 'Odisha',
	 'Punjab',
	 'Rajasthan',
	 'Sikkim',
	 'Tamil Nadu',
	 'Tripura',
	 'Uttarakhand',
	 'Uttar Pradesh',
	 'West Bengal'
 ];
  List<Icon>myicons=[
    Icon(Icons.person),Icon(Icons.calendar_today),Icon(Icons.add_location),
    Icon(Icons.location_searching),Icon(Icons.location_city),Icon(Icons.my_location)];
  String userid="";
  TextEditingController name=TextEditingController();
  TextEditingController price=TextEditingController();
  TextEditingController city=TextEditingController();
  TextEditingController country=TextEditingController();
  TextEditingController pincode=TextEditingController();
  TextEditingController about=TextEditingController();
  checkvalidation(){
    if(name.text.toString().length<1 || about.text.toString().length<1 ||
      pincode.text.toString().length<1  ||
      price.text.toString().length<1 || city.text.toString().length<1 ||
      country.text.toString().length<1)
    {
      _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text("* fields should be filled",style: TextStyle( fontSize: 18,
       color: Colors.white,),), duration: Duration(seconds: 2),));
      return 0;
    }
    return 1;
  }
  Future<String> storeindatabase() async{
     String uri = 'https://aqueous-citadel-72712.herokuapp.com/postaddstableroutes/insertrecord';
     Map<String, String> headers = {"Content-type": "application/json"};
      Map<String,String> body =
      {
      "name":name.text.toString(),
      "age":"0",
      "categoryname":currentcategoryname.toString(),
      "photopostpath": "https://i.ibb.co/Ch45MWF/questionmark.jpg",
      "price":price.text.toString(),
      "likes": "0",
      "userid": userid,
      "city": city.text.toString(),
      "state":currentstate.toString(),
      "country":country.text.toString(),
      "pincode":pincode.text.toString(),
      "about":about.text.toString(),
      "days":currentday.toString(),
      "hours":currenthours.toString(),
      "minutes":currentminutes.toString(),
      "uploadername":username
      };
    String jsonBody = json.encode(body);
    final encoding = Encoding.getByName('utf-8');
    Response response = await post(
    uri,
    headers: headers,
    body: jsonBody,
    encoding: encoding,);
    var jsonData=json.decode(response.body);
    if(jsonData.containsKey("affectedRows"))
    {
        return "Post Add Success";
    }
    else if(jsonData.containsKey("sqlMessage"))
    {
        return "Sorry Try Again";
    }
    else{
      return "Check Internet Connection";
    }
  }

final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
     getNamePreferences().then(updateuserid); 
     getNamePreferences2().then(updateusername); 
     super.initState();
   }
   void updateuserid(String userid){
    setState(() {
      this.userid=userid;
    });
  }
  void updateusername(String userid){
    setState(() {
      this.username=username;
    });
  }
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Post New Service "),
      ),
      body: Container(
        child: new SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Text(
                  "* fields are compulsary",
                  style: TextStyle(
                    fontSize: 10,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Padding(
                      padding: EdgeInsets.only(top: 19,left: 10),
                      child: TextField(
                        controller: name,
                        decoration: InputDecoration(
                          hintText: naming[0],
                          border: InputBorder.none,
                          icon: myicons[0],
                        ),
                      ),
                    )
                  ),
                ),
              ),


              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 15,right: 10,top: 16),
                          child: Text("Category ",
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.black54,
                          ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 19,left: 10),
                          child: DropdownButton<String>(
                            items: category.map((String dropdownStringItem){
                              return DropdownMenuItem<String>(
                                value: dropdownStringItem,
                                child: Text(dropdownStringItem),
                              );
                            }).toList(),
                            onChanged: (String newValueSelected){
                              setState(() {
                                this.currentcategoryname=newValueSelected;
                              });
                            },
                            value: currentcategoryname,
                          ),
                        ),
                      ],
                    )
                  ),
                ),
              ),


              //phone------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Padding(
                      padding: EdgeInsets.only(top: 19,left: 10),
                      child: TextField(
                        controller: price,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          hintText: naming[1],
                          border: InputBorder.none,
                          icon: myicons[1],
                        ),
                      ),
                    )
                  ),
                ),
              ),

              //confirmpassord------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Padding(
                      padding: EdgeInsets.only(top: 19,left: 10),
                      child: TextField(
                        controller: city,
                        decoration: InputDecoration(
                          hintText: naming[2],
                          border: InputBorder.none,
                          icon: myicons[2],
                        ),
                      ),
                    )
                  ),
                ),
              ),

              //gender------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 15,right: 10,top: 16),
                          child: Text("State ",
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.black54,
                          ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 19,left: 10),
                          child: DropdownButton<String>(
                            items: state.map((String dropdownStringItem){
                              return DropdownMenuItem<String>(
                                value: dropdownStringItem,
                                child: Text(dropdownStringItem),
                              );
                            }).toList(),
                            onChanged: (String newValueSelected){
                              setState(() {
                                this.currentstate=newValueSelected;
                              });
                            },
                            value: currentstate,
                          ),
                        ),
                      ],
                    )
                  ),
                ),
              ),

              //city------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Padding(
                      padding: EdgeInsets.only(top: 19,left: 10),
                      child: TextField(
                        controller: country,
                        decoration: InputDecoration(
                          hintText: naming[3],
                          border: InputBorder.none,
                          icon: myicons[3],
                        ),
                      ),
                    )
                  ),
                ),
              ),

              //state------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Padding(
                      padding: EdgeInsets.only(top: 19,left: 10),
                      child: TextField(
                        controller: pincode,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          hintText: naming[4],
                          border: InputBorder.none,
                          icon: myicons[4],
                        ),
                      ),
                    )
                  ),
                ),
              ),

              //country------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Padding(
                      padding: EdgeInsets.only(top: 19,left: 10),
                      child: TextField(
                        controller: about,
                        decoration: InputDecoration(
                          hintText: naming[5],
                          border: InputBorder.none,
                          icon: myicons[5],
                        ),
                      ),
                    )
                  ),
                ),
              ),


              //Pincode------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 15,right: 10,top: 16),
                          child: Text("Rent Days ",
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.black54,
                          ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 19,left: 10),
                          child: DropdownButton<String>(
                            items: rentdays.map((String dropdownStringItem){
                              return DropdownMenuItem<String>(
                                value: dropdownStringItem,
                                child: Text(dropdownStringItem),
                              );
                            }).toList(),
                            onChanged: (String newValueSelected){
                              setState(() {
                                this.currentday=newValueSelected;
                              });
                            },
                            value: currentday,
                          ),
                        ),
                      ],
                    )
                  ),
                ),
              ),

              //Pincode------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 15,right: 10,top: 16),
                          child: Text("Rent Hours ",
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.black54,
                          ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 19,left: 10),
                          child: DropdownButton<String>(
                            items: renthours.map((String dropdownStringItem){
                              return DropdownMenuItem<String>(
                                value: dropdownStringItem,
                                child: Text(dropdownStringItem),
                              );
                            }).toList(),
                            onChanged: (String newValueSelected){
                              setState(() {
                                this.currenthours=newValueSelected;
                              });
                            },
                            value: currenthours,
                          ),
                        ),
                      ],
                    )
                  ),
                ),
              ),


              //Pincode------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 15,right: 10,top: 16),
                          child: Text("Rent Minutes ",
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.black54,
                          ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 19,left: 10),
                          child: DropdownButton<String>(
                            items: rentminutes.map((String dropdownStringItem){
                              return DropdownMenuItem<String>(
                                value: dropdownStringItem,
                                child: Text(dropdownStringItem),
                              );
                            }).toList(),
                            onChanged: (String newValueSelected){
                              setState(() {
                                this.currentminutes=newValueSelected;
                              });
                            },
                            value: currentminutes,
                          ),
                        ),
                      ],
                    )
                  ),
                ),
              ),


              //Age------------
              Padding(
                padding: EdgeInsets.only(top: 25,bottom: 20),
                child: RaisedButton(
                  onPressed: (){
                    if(checkvalidation()==1){
                       storeindatabase().then((onValue){
                         Navigator.pop(context);
                         print(onValue);
                         _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text(onValue,
                         style: TextStyle(
                           fontSize: 16,
                           color: Colors.white,
                         ),
                         ),
                         duration: Duration(seconds: 4),
                         )
                         );
                       });
                       showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return Center(child: CircularProgressIndicator(),);
                      });
                    }
                  },
                  padding: EdgeInsets.only(left: 19,right: 19,top: 13,bottom: 13),
                  child: Text(
                    " Submit "
                  ),
                ),
              ),
            ],
          ),
        ),
      )
    );
  }
}