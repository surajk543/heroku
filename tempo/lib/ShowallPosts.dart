import 'package:flutter/material.dart';

class ShowAllPosts extends StatefulWidget {
  @override
  _ShowAllPostsState createState() => _ShowAllPostsState();
}

class _ShowAllPostsState extends State<ShowAllPosts> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("All Posts"),
      ),
    );
  }
}