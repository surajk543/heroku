import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<String>getNamePreferences() async{
  SharedPreferences prefs= await SharedPreferences.getInstance();
  String name=prefs.getString("userid");
  return name;
}

Future<bool>saveNamePreferences(String userprofilepath) async{
  SharedPreferences prefs= await SharedPreferences.getInstance();
  prefs.setString("userprofilepath", userprofilepath);
  return true;
}

class UploadPhoto extends StatefulWidget {
  @override
  _UploadPhotoState createState() => _UploadPhotoState();
}

class _UploadPhotoState extends State<UploadPhoto> {
  File _image;
  String userid="";
   static final String uploadEndPoint =
      'https://api.imgbb.com/1/upload?key=4f76e18edfa7e7d2a5abc51bae1e62af';
  String base64Image;
  String url="";
  String serverurl="http://aqueous-citadel-72712.herokuapp.com/imageroutes/updateimage";
  Future getImage() async {
  var image = await ImagePicker.pickImage(source: ImageSource.gallery);
  setState(() {
    _image = image;
  });
}
   Future<int>  startupload() async {
    if (_image == null){
      return 0;
    }
   String base64Image = base64Encode(_image.readAsBytesSync());
   String _imageName = _image.path.split("/").last;

   await http.post(uploadEndPoint, body: {
     "image": base64Image,
     "name": _imageName,
   }).then((res) {
     var jsonData=jsonDecode(res.body);
     print(jsonData);
      var temp=jsonData["data"];
      //print(temp);
      String anss=temp["url"].toString();
      //print(anss);
      url=anss;
   }).catchError((err) {
     return 0;
   });
   Map<String, String> headers = {"Content-type": "application/json"};
    final encoding = Encoding.getByName('utf-8');
    Map<String,String>body={
      "userid":userid,
      "userprofilepath":url
    };
    String jsonBody = json.encode(body);
    await http.post(serverurl,body:jsonBody,headers: headers,encoding: encoding).then((res){
      var jsonData=json.decode(res.body);
      if(jsonData.containsKey("affectedRows"))
      {
          saveNamePreferences(url);
          return 1;
      }
      else{
        return 0;
      }
    });
   return 1;
  }
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
     getNamePreferences().then(updateuserid); 
     super.initState();
   }
   void updateuserid(String userid){
    setState(() {
      this.userid=userid;
    });
  }
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Update Photo"),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 87,top: 50),
              child:Container(
                height: 190,width: 190,
                child: ClipOval(
                  child: _image == null ? Text(''):Image.file(_image,fit: BoxFit.fill,),
                ),
              ),
             //child: _image == null ? Text('No image selected.'):Image.file(_image,fit: BoxFit.fill,),
          ),
          Padding(
            padding: EdgeInsets.only(top: 50,left: 100),
            child: Column(
              children: <Widget>[
                FloatingActionButton(
                  onPressed: (){
                    getImage();
                  },
                  child: Icon(Icons.add_a_photo),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 40),
                  child: RaisedButton(
                    onPressed: (){
                      startupload().then((onValue){
                        Navigator.pop(context);
                        print(onValue);
                        if(onValue==1)
                        {
                          _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text("Upload Success",style: TextStyle( fontSize: 18,
                          color: Colors.white,),), duration: Duration(seconds: 2),));
                          
                        }
                        else {
                          _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text("Upload Failed",style: TextStyle( fontSize: 18,
                         color: Colors.white,),), duration: Duration(seconds: 2),));
                        }
                      });
                      showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return Center(child: CircularProgressIndicator(),);});
                    },
                    child: Text(
                      "UPLOAD"
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}