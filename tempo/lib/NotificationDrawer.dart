import 'package:flutter/material.dart';

class NotificationDrawer extends StatefulWidget {
  @override
  _NotificationDrawerState createState() => _NotificationDrawerState();
}

class _NotificationDrawerState extends State<NotificationDrawer> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
      child:Container(
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color:Colors.grey ))
        ),
        child:  InkWell(
          splashColor:Colors.orangeAccent ,
          onTap: ()=>{},
          child: Container(
            height: 55,
            child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(Icons.notifications),
                    Text("    Notification",
                    style:TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
              Icon(Icons.arrow_right),
            ],
          ),
        ),
      ),
      ),
    );
  }
}



