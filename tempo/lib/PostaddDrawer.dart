import 'package:flutter/material.dart';
import 'package:tempo/PostNewService.dart';

class PostaddDrawer extends StatefulWidget {
  @override
  _PostaddDrawerState createState() => _PostaddDrawerState();
}

class _PostaddDrawerState extends State<PostaddDrawer> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
      child:Container(
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color:Colors.grey ))
        ),
        child:  InkWell(
          splashColor:Colors.orangeAccent ,
          onTap: ()=>{
            Navigator.push(context,MaterialPageRoute(builder: (context) => PostNewService(),),),
          },
          child: Container(
            height: 55,
            child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(Icons.redeem),
                    Text("    Post new Service",
                    style:TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
              Icon(Icons.add),
            ],
          ),
        ),
      ),
      ),
    );
  }
}



