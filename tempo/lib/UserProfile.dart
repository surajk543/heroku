import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tempo/UploadPhoto.dart';

Future<String>getNamePreferences() async{
  SharedPreferences prefs= await SharedPreferences.getInstance();
  String name=prefs.getString("email");
  return name;
}
Future<String>getNamePreferences2() async{
  SharedPreferences prefs= await SharedPreferences.getInstance();
  String name=prefs.getString("pass");
  return name;
}
class UserProfile extends StatefulWidget {
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  String urrl="https://aqueous-citadel-72712.herokuapp.com/loginroutes/getbyuserdetails?";
  String email="";
  String pass="";
  Future<UserRecord> __getRecord() async{
    String url=urrl+"email="+email+"&password="+pass;
    var data=await http.get(url);
    var jsonData=jsonDecode(data.body);
    //print(jsonData);
    UserRecord users=UserRecord(jsonData[0]["name"],jsonData[0]["email"],jsonData[0]["country"],jsonData[0]["pincode"],jsonData[0]["phone"],jsonData[0]["gender"]
    ,jsonData[0]["age"],jsonData[0]["state"],jsonData[0]["city"],jsonData[0]["userprofilepath"]);
    print(UserRecord);
    return users;
  }
  @override
  void initState() {
     getNamePreferences().then(updateemail);
     getNamePreferences2().then(updatepass); 
     super.initState();
   }
   void updateemail(String email){
    setState(() {
      this.email=email;
    });
  }
  void updatepass(String pass){
    setState(() {
      this.pass=pass;
    });
  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
      ),
      body: Column(
        children: <Widget>[
          FutureBuilder(
            future: __getRecord(),
            builder: (BuildContext context,AsyncSnapshot snapshot ){
              if(snapshot.data==null){
                return Container(
                  child:Center(child: Text(
                    "Loading...",
                    style: TextStyle(
                      fontSize: 19
                    ),
                  ))
                );
              }
              else{
                return  Container(
                  height: 580,
                  child: ListView(
                      scrollDirection: Axis.vertical,
                      children: <Widget>[
                        Container(
                          width: 250,
                          height: 250,
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.only(top: 15,left: 10),
                              child: Image(
                                image: NetworkImage(snapshot.data.userprofilepath),
                              ),
                            ),
                          ),
                        ),

                        Container(
                          width: 300,
                          height: 60,
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.only(top: 15,left: 10),
                              child: Text(
                                "Name : ${snapshot.data.name}",
                                style: TextStyle(
                                  fontSize: 20
                                ),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          width: 300,
                          height: 60,
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.only(top: 15,left: 10),
                              child: Text(
                                "Email : ${snapshot.data.email}",
                                style: TextStyle(
                                  fontSize: 20
                                ),
                              ),
                            ),
                          ),
                        ),

                        Container(
                          width: 300,
                          height: 60,
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.only(top: 15,left: 10),
                              child: Text(
                                "Age : ${snapshot.data.age}",
                                style: TextStyle(
                                  fontSize: 20
                                ),
                              ),
                            ),
                          ),
                        ),

                        Container(
                          width: 300,
                          height: 60,
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.only(top: 15,left: 10),
                              child: Text(
                                "City : ${snapshot.data.city}",
                                style: TextStyle(
                                  fontSize: 20
                                ),
                              ),
                            ),
                          ),
                        ),

                        Container(
                          width: 300,
                          height: 60,
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.only(top: 15,left: 10),
                              child: Text(
                                "pincode : ${snapshot.data.pincode}",
                                style: TextStyle(
                                  fontSize: 20
                                ),
                              ),
                            ),
                          ),
                        ),

                        Container(
                          width: 300,
                          height: 60,
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.only(top: 15,left: 10),
                              child: Text(
                                "state : ${snapshot.data.state}",
                                style: TextStyle(
                                  fontSize: 20
                                ),
                              ),
                            ),
                          ),
                        ),

                        Container(
                          width: 300,
                          height: 60,
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.only(top: 15,left: 10),
                              child: Text(
                                "country : ${snapshot.data.country}",
                                style: TextStyle(
                                  fontSize: 20
                                ),
                              ),
                            ),
                          ),
                        ),

                        Container(
                          width: 300,
                          height: 60,
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.only(top: 15,left: 10),
                              child: Text(
                                "Gender : ${snapshot.data.gender}",
                                style: TextStyle(
                                  fontSize: 20
                                ),
                              ),
                            ),
                          ),
                        ),

                        Container(
                          width: 300,
                          height: 60,
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.only(top: 15,left: 10),
                              child: Text(
                                "Phone : ${snapshot.data.phone}",
                                style: TextStyle(
                                  fontSize: 20
                                ),
                              ),
                            ),
                          ),
                        ),

                        
                             Padding(
                              padding: EdgeInsets.only(top: 15,left: 95,right: 95),
                              child:RaisedButton(
                                onPressed: (){
                                  Route route=MaterialPageRoute(builder: (context)=>UploadPhoto());
                                  Navigator.push(context, route);
                                },
                                child: Text(
                                  "UPLOAD PHOTO"
                                ),
                              ),
                            ), 


                      ],
                    ),
                );
              }
            }
          )
        ],
      )
    );
  }
}

class UserRecord
{
  final String name;
  final String email;
  final String country;
  final String pincode;
  final String phone;
  final String gender;
  final String age;
  final String state;
  final String city;
  final userprofilepath;
  UserRecord(this.name,this.email,this.country,this.pincode,this.phone
  ,this.gender,this.age,this.state,this.city,this.userprofilepath);
}