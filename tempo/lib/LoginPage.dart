import 'package:flutter/material.dart';
import 'Menu.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

Future<bool>saveNamePreferences(String email,String password,String userid,String userprofilepath,String name) async{
  SharedPreferences prefs= await SharedPreferences.getInstance();
  prefs.setString("email", email);
  prefs.setString("pass", password);
  prefs.setString("userid", userid);
  prefs.setString("userprofilepath", userprofilepath);
  prefs.setString("name", name);
  return true;
}
Future<String>getNamePreferences() async{
  SharedPreferences prefs= await SharedPreferences.getInstance();
  String name=prefs.getString("email");
  return name;
}
Future<String>getNamePreferences2() async{
  SharedPreferences prefs= await SharedPreferences.getInstance();
  String name=prefs.getString("pass");
  return name;
}
class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController emailstr=TextEditingController();
  TextEditingController passstr=TextEditingController();
  String email="";String pass="";
  String urrl="https://aqueous-citadel-72712.herokuapp.com/loginroutes/getbyuserdetails?";

  Future<int> servercheck() async{
    String url=urrl+"email="+email+"&password="+pass;
    var data=await http.get(url);
    var jsonData=jsonDecode(data.body);
    if(jsonData.length==1){
      String userid=jsonData[0]["userid"];
      String userprofilepath=jsonData[0]["userprofilepath"];
      String name=jsonData[0]["name"];
      saveNamePreferences(email,pass,userid,userprofilepath,name);
      return 1;
    }
    return 0;
  }
  checkvalidation(){
    email=emailstr.text.toString();
    pass=passstr.text.toString();
    if(emailstr.text.toString().length<1 || passstr.text.toString().length<1)
    {
         _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text("Email/Password should be filled",style: TextStyle( fontSize: 18,
       color: Colors.white,),), duration: Duration(seconds: 2),));
       return 0;
    }
    return 1;
  }
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override 
  void initState() {
    getNamePreferences().then(updateemail);
     getNamePreferences2().then(updatepass); 
    super.initState();
  }
  void updateemail(String email){
    setState(() {
      this.email=email;
    });
  }
  void updatepass(String pass){
    setState(() {
      this.pass=pass;
      if(email.length>0 && pass.length>0)
      {
          servercheck().then((onValue){
          Navigator.pop(context);
          if(onValue==1){
          Route route=MaterialPageRoute(builder: (context)=>Menu());
          Navigator.push(context, route);
          }
          else{
           _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text("Wrong Details",style: TextStyle( fontSize: 18,
           color: Colors.white,),), duration: Duration(seconds: 2),));
          }
          });
          showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
          return Center(child: CircularProgressIndicator(),);
          });
      }
    });
  }
  Widget build(BuildContext context) {
    return Scaffold(
       key: _scaffoldKey,
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      body: Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 10),
            child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  colors: <Color>[
                  Colors.orange[900],
                  Colors.orange[800],
                  Colors.orange[400],
                ]),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 210),
            child: Container(
              height: 729,width: 395,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(50),topRight: Radius.circular(50))
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 120,left: 15),
            child: Text(
              "Login",
              style:TextStyle(
                fontSize: 47,
                color: Colors.white,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 180,left: 15),
            child: Text(
              "Welcome Back",
              style:TextStyle(
                fontSize: 16,
                color: Colors.white,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 250,left: 15),
            child:Container(
              height: 70,
              width: 360,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [BoxShadow(
                  color: Color.fromRGBO(225, 95, 27, 3),
                  blurRadius: 20,
                  offset: Offset(0,10)
                )]
              ),
              child:Padding(
                padding:EdgeInsets.only(top: 15,left: 10,right: 10),
                child: TextField(
                  controller: emailstr,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    icon: Icon(Icons.email),
                    hintText: "Email or Username", 
                  ),
                ),
              ),
            )
          ),

          Padding(
            padding: EdgeInsets.only(top: 340,left: 15),
            child:Container(
              height: 70,
              width: 360,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [BoxShadow(
                  color: Color.fromRGBO(225, 95, 27, 3),
                  blurRadius: 20,
                  offset: Offset(0,10)
                )]
              ),
              child:Padding(
                padding:EdgeInsets.only(top: 15,left: 10,right: 10),
                child: TextField(
                  controller: passstr,
                  obscureText: true,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    icon: Icon(Icons.lock),
                    hintText: "Password", 
                  ),
                ),
              ),
            )
          ),
          
          Padding(
            padding: EdgeInsets.only(top: 490,left: 240),
            child: FlatButton(
              onPressed: (){},
              child: Text(
                "Forgot Password ?",
                style: TextStyle(
                  color: Colors.black54,
                ),
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(top: 430,left: 120),
            child: RaisedButton(
              onPressed: (){
                if(checkvalidation()==1)
                {
                    servercheck().then((onValue){
                      Navigator.pop(context);
                      if(onValue==1){
                        Route route=MaterialPageRoute(builder: (context)=>Menu());
                        Navigator.push(context, route);
                      }
                      else{
                         _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text("Wrong Details",style: TextStyle( fontSize: 18,
                         color: Colors.white,),), duration: Duration(seconds: 2),));
                      }
                    });
                    showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return Center(child: CircularProgressIndicator(),);
                    });
                }
              },
              elevation: 10,
              padding: EdgeInsets.only(left: 40,right: 40,top: 12,bottom: 12),
              child: Text(
                " Submit ",
                style: TextStyle(
                  color: Colors.black54,
                  fontSize: 18,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
/*
Route route=MaterialPageRoute(builder: (context)=>Menu());
                Navigator.push(context, route);
*/