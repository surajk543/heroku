import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

Future<String>getNamePreferences() async{
  SharedPreferences prefs= await SharedPreferences.getInstance();
  String name=prefs.getString("userid");
  return name;
}

class YourOwnPost extends StatefulWidget {
  @override
  _YourOwnPostState createState() => _YourOwnPostState();
}

class _YourOwnPostState extends State<YourOwnPost> {
  String urrl="http://aqueous-citadel-72712.herokuapp.com/postaddstableroutes/getbyuserid?userid=";
  String userid="";
  Future<List<PostaddsCar>> __getUsers() async{
    String url=urrl+userid;
    var data=await http.get(url);
    var jsonData=jsonDecode(data.body);
    List<PostaddsCar> users=[];
    for(var u in jsonData){
      PostaddsCar user= PostaddsCar(u["name"],u["price"],u["categoryname"],u["photopostpath"],u["uploadername"],u["createdon"]);
      users.add(user);
    }
    print(users.length);
    return users;
  }
  @override
void initState() {
     getNamePreferences().then(updateuserid); 
     super.initState();
   }
  void updateuserid(String userid){
    setState(() {
      this.userid=userid;
    });
  }
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    final double itemWidth = size.width / 2;
    return Scaffold(
      appBar: AppBar(
        title: Text("Posts List"),
      ),
      body: Container(
        child: FutureBuilder(
          future: __getUsers(),
          builder: (BuildContext context,AsyncSnapshot snapshot){
            if(snapshot.data==null)
            {
                return Container(
                  child: Center(
                    child: Text(
                      "Loading...",
                    ),
                  ),
                );
            }
            else{
              return GridView.count(
                // Create a grid with 2 columns. If you change the scrollDirection to
                // horizontal, this produces 2 rows.
                crossAxisCount: 2,
                childAspectRatio: (itemWidth / itemHeight),
                // Generate 100 widgets that display their index in the List.
                children: List.generate(snapshot.data.length, (index) {
                  return Card(
                    child:InkWell(
                      splashColor: Colors.black54,
                      onTap: (){},
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Container(
                                width:itemWidth*0.9589,
                                height: itemHeight*0.76,
                                child: Image(
                                  image: NetworkImage(snapshot.data[index].photopostpath),
                                  fit: BoxFit.fill,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 248),
                                child: Banner(
                                  message: snapshot.data[index].categoryname,
                                  location: BannerLocation.topEnd,
                                ),
                              )
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 1),
                              child:Text(
                                snapshot.data[index].name,
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontWeight:FontWeight.w500
                                ),
                              ),
                            ),

                            Padding(
                              padding: EdgeInsets.only(top: 1),
                              child:Text(
                                "Posted By:${snapshot.data[index].uploadername}",
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.black54,
                                ),
                              ),
                            ),

                          Padding(
                              padding: EdgeInsets.only(top: 1),
                              child:Text(
                                "Price: Rs ${snapshot.data[index].price}",
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                ),
                              ),
                            ),

                          Padding(
                              padding: EdgeInsets.only(top: 1),
                              child:Text(
                                "Posted Date: ${snapshot.data[index].createdon.substring(0,10)}",
                                style: TextStyle(
                                  fontSize: 8,
                                  color: Colors.black,
                                ),
                              ),
                            )
                        ],
                      ),
                    ),
                  );
                }),
              );
            }
          },
        ),
      ),
    );
  }
}
class PostaddsCar
{
  final String name;
  final String price;
  final String categoryname;
  final String photopostpath;
  final String uploadername;
  final String createdon;
  PostaddsCar(this.name,this.price,this.categoryname,this.photopostpath,this.uploadername,this.createdon);
}