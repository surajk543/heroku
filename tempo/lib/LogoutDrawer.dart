import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
Future<bool>saveNamePreferences() async{
  SharedPreferences prefs= await SharedPreferences.getInstance();
  prefs.setString("email", "");
  prefs.setString("pass", "");
  prefs.setString("userid", "");
  prefs.setString("userprofilepath", "");
  prefs.setString("name", "");
  return true;
}
class LogoutDrawer extends StatefulWidget {
  @override
  _LogoutDrawerState createState() => _LogoutDrawerState();
}

class _LogoutDrawerState extends State<LogoutDrawer> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
      child:Container(
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color:Colors.grey ))
        ),
        child:  InkWell(
          splashColor:Colors.orangeAccent ,
          onTap: ()=>{
            saveNamePreferences(),
            Navigator.pop(context),
            Navigator.pop(context),
          },
          child: Container(
            height: 55,
            child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(Icons.lock_open),
                    Text("    Logout",
                    style:TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
              Icon(Icons.arrow_right),
            ],
          ),
        ),
      ),
      ),
    );
  }
}