import 'package:flutter/material.dart';
import 'package:tempo/CarList.dart';
import 'package:tempo/PostNewService.dart';

class MenuHorizontalScroll extends StatefulWidget {
  @override
  _MenuHorizontalScrollState createState() => _MenuHorizontalScrollState();
}

class _MenuHorizontalScrollState extends State<MenuHorizontalScroll> {
  final List<String> numbers = ["assets/1.jpg","assets/2.jpg","assets/3.jpg","assets/4.jpg","assets/5.jpg"];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        child: Column(
          children: <Widget>[
             Container(
               height: 260.0,
               child: ListView(
                 scrollDirection: Axis.horizontal,
                 children: <Widget>[
                   Container(
                     width: 260,
                     child: Card(
                       child:Ink.image(
                         image: AssetImage(numbers[0]),
                          fit: BoxFit.cover,
                          child: InkWell(
                            onTap: (){
                              Route route=MaterialPageRoute(builder: (context)=>CarList());
                              Navigator.push(context, route);
                            },
                          ),
                       ),
                     ),
                   ),
                   Container(
                     width: 260,
                     child: Card(
                       child:Ink.image(
                         image: AssetImage(numbers[1]),
                          fit: BoxFit.cover,
                          child: InkWell(
                            onTap: (){},
                          ),
                       ),
                     ),
                   ),

                   Container(
                     width: 260,
                     child: Card(
                       child:Ink.image(
                         image: AssetImage(numbers[2]),
                          fit: BoxFit.cover,
                          child: InkWell(
                            onTap: (){},
                          ),
                       ),
                     ),
                   ),

                   Container(
                     width: 260,
                     child: Card(
                       child:Ink.image(
                         image: AssetImage(numbers[3]),
                          fit: BoxFit.cover,
                          child: InkWell(
                            onTap: (){},
                          ),
                       ),
                     ),
                   ),

                   Container(
                     width: 260,
                     child: Card(
                       child:Ink.image(
                         image: AssetImage(numbers[4]),
                          fit: BoxFit.cover,
                          child: InkWell(
                            onTap: (){},
                          ),
                       ),
                     ),
                   ),
                 ],
               ),
             ),
            
             Padding(
               padding: EdgeInsets.only(top: 300,left: 280),
               child: FloatingActionButton(
                 onPressed: (){
                   Route route=MaterialPageRoute(builder: (context)=>PostNewService());
                   Navigator.push(context, route);
                 },
                 child: Icon(Icons.add),
               ),
             ),
          ],
        ),
      ),
    );
  }
}
/*
Scaffold(
      body:Padding(
      padding: EdgeInsets.only(bottom: 420),
      child: Container(
      padding: EdgeInsets.symmetric(horizontal: 16,vertical: 2),
      child: ListView.builder(
        itemCount: 5,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
        return Container(
          width: MediaQuery.of(context).size.width*0.6,
          child: Card(
            //color: Colors.blue,
            child: Ink.image(
              image: AssetImage(numbers[index]),
              fit: BoxFit.cover,
              child: InkWell(
                onTap: (){
                  /*Route route=MaterialPageRoute(builder: (context)=>CarList());
                      Navigator.push(context, route);*/
                },
              ),
            ),
          ),
        );
       },
      ),
    ),
    ),
    );
    */