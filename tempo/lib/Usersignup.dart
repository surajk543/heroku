import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart';

Future<bool>saveNamePreferences() async{
  SharedPreferences prefs= await SharedPreferences.getInstance();
  prefs.setString("userprofilepath", "https://i.ibb.co/SRypvcM/userpath.jpg");
  return true;
}

class Usersignup extends StatefulWidget {
  @override
  _UsersignupState createState() => _UsersignupState();
}

class _UsersignupState extends State<Usersignup> {
  List<String>naming=["Name*","Age*","Email*","Phone","Password*","Confirm Password*","City","Country",
  "Pincode","Userprofilepath"];
  var current="Male";
  var gender=["Male","Female"];
  var currentstate='Andhra Pradesh';
  var state=[
	 'Andhra Pradesh',
	 'Arunachal Pradesh',
	 'Assam',
	 'Bihar',
	 'Chhattisgarh',
	 'Goa',
	 'Gujarat',
	 'Haryana',
	 'Himachal Pradesh',
	 'Jammu & Kashmir',
	 'Jharkhand',
	 'Karnataka',
	 'Kerala',
	 'Madhya Pradesh',
	 'Maharashtra',
	 'Manipur',
	 'Meghalaya',
	 'Mizoram',
	 'Nagaland',
	 'Odisha',
	 'Punjab',
	 'Rajasthan',
	 'Sikkim',
	 'Tamil Nadu',
	 'Tripura',
	 'Uttarakhand',
	 'Uttar Pradesh',
	 'West Bengal'
 ];
  List<Icon>myicons=[
    Icon(Icons.person),Icon(Icons.play_circle_outline),Icon(Icons.email),Icon(Icons.phone),Icon(Icons.lock_outline),
    Icon(Icons.lock_outline),Icon(Icons.person_outline),Icon(Icons.location_city),Icon(Icons.location_searching),
    Icon(Icons.add_location),Icon(Icons.edit_location)
  ];
  TextEditingController name=TextEditingController();
  TextEditingController age=TextEditingController();
  TextEditingController email=TextEditingController();
  TextEditingController phone=TextEditingController();
  TextEditingController password=TextEditingController();
  TextEditingController city=TextEditingController();
  TextEditingController country=TextEditingController();
  TextEditingController pincode=TextEditingController();
  TextEditingController confirmpassword=TextEditingController();
  checkvalidation(){
    if(name.text.toString().length<1 || age.text.toString().length<1
      || email.text.toString().length<1 || password.text.toString().length<1 ||
      confirmpassword.text.toString().length<1)
    {
      _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text("* fields should be filled",style: TextStyle( fontSize: 18,
       color: Colors.white,),), duration: Duration(seconds: 2),));
      return 0;
    }
    else if(password.text.toString().compareTo(confirmpassword.text.toString())!=0){
       _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text("Password not matched",style: TextStyle( fontSize: 14,
       color: Colors.white,),), duration: Duration(seconds: 2),));
      return 0;
    }
    else if(password.text.toString().length<5)
    {
       _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text("length of Password should contain more than 5 characters",style: TextStyle( fontSize: 14,
       color: Colors.white,),), duration: Duration(seconds: 2),));
        return 0;
    }
    return 1;
  }
  Future<String> storeindatabase() async{
     String uri = 'https://aqueous-citadel-72712.herokuapp.com/usertableroutes/insertrecord';
     Map<String, String> headers = {"Content-type": "application/json"};
      Map<String,String> body =
      {
      "name":name.text.toString(),
      "age":age.text.toString(),
      "email":email.text.toString(),
      "phone":phone.text.toString(),
      "password":password.text.toString(),
      "gender":current.toString(),
      "city": city.text.toString(),
      "state": currentstate.toString(),
      "country":country.text.toString(),
      "pincode":pincode.text.toString(),
      "userprofilepath":"https://i.ibb.co/SRypvcM/userpath.jpg"
    };
    String jsonBody = json.encode(body);
    final encoding = Encoding.getByName('utf-8');
    Response response = await post(
    uri,
    headers: headers,
    body: jsonBody,
    encoding: encoding,);
    var jsonData=json.decode(response.body);
    if(jsonData.containsKey("affectedRows"))
    {
        return "Registarion Success";
    }
    else if(jsonData.containsKey("sqlMessage"))
    {
        return "Account Already Exist";
    }
    else{
      return "Check Internet Connection";
    }
  }

final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Registration Form "),
      ),
      body: Container(
        child: new SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Text(
                  "* fields are compulsary",
                  style: TextStyle(
                    fontSize: 10,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Padding(
                      padding: EdgeInsets.only(top: 19,left: 10),
                      child: TextField(
                        controller: name,
                        decoration: InputDecoration(
                          hintText: naming[0],
                          border: InputBorder.none,
                          icon: myicons[0],
                        ),
                      ),
                    )
                  ),
                ),
              ),

              //Age------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Padding(
                      padding: EdgeInsets.only(top: 19,left: 10),
                      child: TextField(
                        controller: age,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          hintText: naming[1],
                          border: InputBorder.none,
                          icon: myicons[1],
                        ),
                      ),
                    )
                  ),
                ),
              ),

              //Email------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Padding(
                      padding: EdgeInsets.only(top: 19,left: 10),
                      child: TextField(
                        controller: email,
                        decoration: InputDecoration(
                          hintText: naming[2],
                          border: InputBorder.none,
                          icon: myicons[2],
                        ),
                      ),
                    )
                  ),
                ),
              ),

              //phone------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Padding(
                      padding: EdgeInsets.only(top: 19,left: 10),
                      child: TextField(
                        controller: phone,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          hintText: naming[3],
                          border: InputBorder.none,
                          icon: myicons[3],
                        ),
                      ),
                    )
                  ),
                ),
              ),

              //password------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Padding(
                      padding: EdgeInsets.only(top: 19,left: 10),
                      child: TextField(
                        controller: password,
                        obscureText: true,
                        decoration: InputDecoration(
                          hintText: naming[4],
                          border: InputBorder.none,
                          icon: myicons[4],
                        ),
                      ),
                    )
                  ),
                ),
              ),

              //confirmpassord------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Padding(
                      padding: EdgeInsets.only(top: 19,left: 10),
                      child: TextField(
                        controller: confirmpassword,
                        obscureText: true,
                        decoration: InputDecoration(
                          hintText: naming[5],
                          border: InputBorder.none,
                          icon: myicons[5],
                        ),
                      ),
                    )
                  ),
                ),
              ),

              //DropDown------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 15,right: 10,top: 16),
                          child: Text("Gender ",
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.black54,
                          ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 19,left: 10),
                          child: DropdownButton<String>(
                            items: gender.map((String dropdownStringItem){
                              return DropdownMenuItem<String>(
                                value: dropdownStringItem,
                                child: Text(dropdownStringItem),
                              );
                            }).toList(),
                            onChanged: (String newValueSelected){
                              setState(() {
                                this.current=newValueSelected;
                              });
                            },
                            value: current,
                          ),
                        ),
                      ],
                    )
                  ),
                ),
              ),

              //city------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Padding(
                      padding: EdgeInsets.only(top: 19,left: 10),
                      child: TextField(
                        controller: city,
                        decoration: InputDecoration(
                          hintText: naming[6],
                          border: InputBorder.none,
                          icon: myicons[6],
                        ),
                      ),
                    )
                  ),
                ),
              ),

              //state------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 15,right: 10,top: 16),
                          child: Text("State ",
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.black54,
                          ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 19,left: 10),
                          child: DropdownButton<String>(
                            items: state.map((String dropdownStringItem){
                              return DropdownMenuItem<String>(
                                value: dropdownStringItem,
                                child: Text(dropdownStringItem),
                              );
                            }).toList(),
                            onChanged: (String newValueSelected){
                              setState(() {
                                this.currentstate=newValueSelected;
                              });
                            },
                            value: currentstate,
                          ),
                        ),
                      ],
                    )
                  ),
                ),
              ),


              //country------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Padding(
                      padding: EdgeInsets.only(top: 19,left: 10),
                      child: TextField(
                        controller: country,
                        decoration: InputDecoration(
                          hintText: naming[7],
                          border: InputBorder.none,
                          icon: myicons[7],
                        ),
                      ),
                    )
                  ),
                ),
              ),


              //Pincode------------
              Padding(
                padding: EdgeInsets.only(top: 1),
                child: Container(
                  height: 85,width: 390,
                  child: Card(
                    child:Padding(
                      padding: EdgeInsets.only(top: 19,left: 10),
                      child: TextField(
                        controller: pincode,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          hintText: naming[8],
                          border: InputBorder.none,
                          icon: myicons[8],
                        ),
                      ),
                    )
                  ),
                ),
              ),

              //Age------------
              Padding(
                padding: EdgeInsets.only(top: 25,bottom: 20),
                child: RaisedButton(
                  onPressed: (){
                    saveNamePreferences();
                    if(checkvalidation()==1){
                       storeindatabase().then((onValue){
                         Navigator.pop(context);
                         print(onValue);
                         _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text(onValue,
                         style: TextStyle(
                           fontSize: 16,
                           color: Colors.white,
                         ),
                         ),
                         duration: Duration(seconds: 4),
                         )
                         );
                       });
                       showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return Center(child: CircularProgressIndicator(),);
                      });
                    }
                  },
                  padding: EdgeInsets.only(left: 19,right: 19,top: 13,bottom: 13),
                  child: Text(
                    " Submit "
                  ),
                ),
              ),
            ],
          ),
        ),
      )
    );
  }
}