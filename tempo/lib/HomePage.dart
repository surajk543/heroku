import 'package:flutter/material.dart';
import 'package:tempo/Usersignup.dart';
import 'LoginPage.dart';
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController customControlleremail= TextEditingController();
  TextEditingController customControllerpassword= TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      body: Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 300,left: 130),
            child:Container(
            width: 130,
            height: 40,
            child: RaisedButton(
              onPressed: ()  {
                Route route=MaterialPageRoute(builder: (context)=>LoginPage());
                Navigator.push(context, route);
              },
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10),
              ),
              color: Colors.blue[800],
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 12),
                    child:Icon(Icons.email,color: Colors.white),
                  ),
                  Text(
                    "Login",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
          ),
          
          //Second Button--------------------------------------

          Padding(
            padding: EdgeInsets.only(top: 370,left: 130),
            child:Container(
            width: 135,
            height: 40,
            child: RaisedButton(
              onPressed: (){
                Route route=MaterialPageRoute(builder: (context)=>Usersignup());
                Navigator.push(context, route);
              },
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10),
              ),
              color: Colors.blue[800],
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 12),
                    child:Icon(Icons.email,color: Colors.white),
                  ),
                  Text(
                    "Sign Up",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
          ),

        ]
      ),
    );
  }
}