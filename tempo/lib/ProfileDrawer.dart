import 'package:flutter/material.dart';
import 'package:tempo/UserProfile.dart';

class ProfileDrawer extends StatefulWidget {
  @override
  _ProfileDrawerState createState() => _ProfileDrawerState();
}

class _ProfileDrawerState extends State<ProfileDrawer> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
      child:Container(
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color:Colors.grey ))
        ),
        child:  InkWell(
          splashColor:Colors.orangeAccent ,
          onTap: ()=>{
            Navigator.push(context,MaterialPageRoute(builder: (context) => UserProfile(),),),
          },
          child: Container(
            height: 47,
            child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(Icons.person),
                    Text("    Profile",
                    style:TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
              Icon(Icons.arrow_right),
            ],
          ),
        ),
      ),
      ),
    );
  }
}
